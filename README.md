# pytube


### Goal

Provides a small youtube player in Python, useful for having a micro player

#### Steps of producing this project

1 - Installing pipenv as user 

`pip install --user pipenv`


If not available through console (MAC) :

`Add binary path to PATH`

`export PY_USER_BIN=$(python -c 'import site; print(site.USER_BASE + "/bin")')`

`export PATH=$PY_USER_BIN:$PATH`

*http://matthew-brett.github.io/pydagogue/installing_on_debian.html*

2 - 



#### Changelog

At beginning 
- Structure
- Authorization from Google API
- Learning and implementing pipenv project organization